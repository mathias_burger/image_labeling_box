# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "boxcutter/ubuntu1604-desktop"
  config.vm.network "forwarded_port", guest: 10081, host: 10081
  config.vm.synced_folder "./data", "/vagrant_data"

  config.vm.provider "virtualbox" do |vb|
    vb.gui = true
    vb.memory = "2048"
    vb.customize ["modifyvm", :id, "--cableconnected1", "on"]
  end

  config.vm.provision "file", source: "./files/home/vagrant/Desktop", destination: "~/"
  config.vm.provision "shell", privileged: true, inline: <<-SHELL
    update-locale LANG=en_US.UTF-8 LC_MESSAGES=POSIX
    apt-get update
    apt-get install -y \
      docker.io \
      python python-pip python-numpy python-qt4 python-pil python-lxml
    usermod -aG docker vagrant
  SHELL
  config.vm.provision "shell", privileged: false, inline: <<-SHELL
    sloth_dir=~/sloth
    mkdir -p $sloth_dir
    git clone https://github.com/cvhciKIT/sloth $sloth_dir
    cd $sloth_dir
    sudo python2 setup.py install

    pip2 install labelImg

    labelme_dir=~/labelme
    git clone https://github.com/CSAILVision/LabelMeAnnotationTool.git $labelme_dir
    cd $labelme_dir
    sudo docker build -t labelme .

    sudo reboot
  SHELL
end
