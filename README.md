## Setup

### Download and setup software

* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant](https://www.vagrantup.com/downloads.html)

### Setup machine

Setup your shared folder on the host system

```
# as link
ln -s <path-to-your-files> data
# or create data dir to be populated later
mkdir data
```

Boot the machine

```
vagrant up
```

Stop the machine

```
vagrant halt
```

### Inside the machine

All important applications have starter icons.

The *data* directory on the host system is available as */vagrant_data* on the guest.

LabelMe expects images to be in the folder *~/labelme/Images/<dataset_folder>*. You may link the shared data directory 
using a command like *cd ~/labelme/Images && ln -s /vagrant_data dataset*.